<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use App\Services\FileUploader;
use App\Services\Notification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/post", name="post.")
 */
class PostController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param PostRepository $postRepository
     * @return Response
     */
    public function index(PostRepository $postRepository): Response
    {
        $posts = $postRepository->findAll();
        dump($posts);

        if(!empty($posts)){
            return $this->render('post/index.html.twig', [
                'posts' => $posts,
            ]);
        }
        //return new JsonResponse($posts);

    }

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @param FileUploader $fileUploader
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function create (Request $request, FileUploader $fileUploader, Notification $notification){

        $post = new Post();
        //$post->setTitle('This is going tobe a title 7');
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            //dump($request->files);
            /**
             * @var UploadedFile $file
             */

            $file = $request->files->get('post')['attachment'];
            if($file){
                $filename = $fileUploader->uploadFile($file);
//                $filename = md5(uniqid()). '.' . $file->guessClientExtension();
//
//                $file->move(
//                    $this->getParameter('uploads_dir'),
//                    $filename
//                );
                $post->setImage($filename);
                //dump($post);
                $em->persist($post);
                //save to db
                $em->flush();

            }


            $this->addFlash('notice', 'New Post added');

            return $this->redirectToRoute('post.index');
        }

        //return a response



        //return new Response('Post was created' );
        return $this->render('/post/create.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * @route("/show/{id}", name="show")
     * @param $id
     * @return Response
     */

    public function show($id, PostRepository $postRepository){
        $post = $postRepository->find($id);
        dump($post);

        return $this->render('/post/show.html.twig',[
            'post' => $post,
        ]);
    }

    /**
     * @route("/delete/{id}", name="delete")
     * @param $id
     * @return Response
     */

    public function remove($id, PostRepository $postRepository): Response{

        $post_d = $postRepository->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($post_d);

        $em->flush();

        $this->addFlash('notice', 'The post has been deleted');

        return $this->redirectToRoute('post.index');
    }

    /**
     * @param $id
     * @param PostRepository $postRepository
     * @return Response
     */
//    public function update($id, PostRepository $postRepository): Response{
//        $post_u = $postRepository->find($id);
//        $em = $this->getDoctrine()->getManager();
//
//        if (!$post_u) {
//            throw $this->createNotFoundException(
//                'No product found for id '.$id
//            );
//        }
//
//
//    }

}
