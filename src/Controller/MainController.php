<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{

    public function index(): Response
    {
//        return $this->json([
//            'message' => 'Welcome to your new controller!',
//            'path' => 'src/Controller/MainController.php',
//        ]);

        //return new Response('<h1>Hello World</h1>');

        return $this->render('home/index.html.twig');
    }

    public function custom(): Response{

        //dump($request);

        return $this->render('home/custom.html.twig');
    }
}
